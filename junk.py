import random
import time
import telepot
from datetime import datetime
from telepot.loop import MessageLoop
from falas_enum import Falas_Enum
from audios_enum import Audios_Enum


TOKEN = '420312421:AAE-JmmjfST6oINHqXCNjYmOht0klQUqxkw'
OWNER_ID = '@PauloGuyss'

BLABLERS = ['328500131', '225790019']


# Método que recebe a mensagem
def handle(msg):
    chat_id = msg['chat']['id']
    username = msg['from']['username']
    definirChatId(chat_id)
    mensagem = msg['text']

    print(msg)

    falas = Falas_Enum
    audios = Audios_Enum

    blablers_list = []
    with open('blablers.txt') as blablers:
       blablers_list = blablers.read().splitlines()

    print(blablers_list)

    if 'imposto' in mensagem:
        bot.sendMessage(chat_id, random.choice(falas.RESPOSTAS_IMPOSTO), reply_to_message_id=msg['message_id'])

    for frase in Falas_Enum.FALAS_BOM_DIA:
        if frase in mensagem.lower():
            bot.sendMessage(chat_id, random.choice(falas.RESPOSTAS_BOM_DIA), reply_to_message_id=msg['message_id'])

    for frase in Falas_Enum.FALAS_XINGAMENTOS:
        if frase in mensagem.lower():
            bot.sendVoice(chat_id, voice=open(random.choice(audios.RESPOSTAS_XINGAMENTOS), 'rb'), reply_to_message_id=msg['message_id'])

    if 'bacon' in mensagem.lower():
        bot.sendMessage(chatID, 'Eita, camarada, parece que o meu comparsa Roadhog leu sua mensagem e encaminhou isso aqui:', reply_to_message_id=msg['message_id'])
        bot.sendVoice(chatID, voice=open('audios/diga_bacon.mp3', 'rb'))

    elif 'guentai' in mensagem.lower() or 'perai' in mensagem.lower():
        bot.sendVoice(chatID, voice=open('audios/anda_anda_anda.mp3', 'rb'), reply_to_message_id=msg['message_id'])

    elif 'vacilei' in mensagem.lower():
        bot.sendVoice(chatID, voice=open('audios/pisou_na_bola.m4a', 'rb'), reply_to_message_id=msg['message_id'])

    elif mensagem.lower() == 'ola' or mensagem.lower() == 'olá' or mensagem.lower() == 'oi' or mensagem.lower() == 'e ai':
        bot.sendMessage(chat_id, random.choice(falas.RESPOSTAS_BEM_VINDO), reply_to_message_id=msg['message_id'])

    elif 'obrigado' in mensagem.lower() or 'thanks' in mensagem.lower() and 'junk' in mensagem.lower():
        bot.sendMessage(chat_id, 'Em vez de agradecer, você poderia pagar em bombas, parça', reply_to_message_id=msg['message_id'])

    elif 'masoq' in mensagem.lower():
        bot.sendMessage(chat_id, 'Em vez de entender, já tentou explodir?', reply_to_message_id=msg['message_id'])

    elif 'teste' in mensagem.lower():
        bot.sendMessage(chat_id, 'Parece que funcionou, camarada', reply_to_message_id=msg['message_id'])

    elif 'roubadão' in mensagem.lower() or 'roubado' in mensagem.lower() or 'roubo' in mensagem.lower () \
            and '@' in mensagem.lower() or 'é' in mensagem.lower() and username in blablers_list:
        bot.sendMessage(chat_id, random.choice(falas.RESPOSTAS_ROUBADAO), reply_to_message_id=msg['message_id'])

    elif mensagem == '/talk':
        bot.sendMessage(chatID, random.choice(falas.RESPOSTAS_ALEATORIAS))

    elif mensagem == '/data':
        bot.sendMessage(chat_id, 'São {0}:{1} do dia {2}/{3}/{4}, camarada'
                        .format(datetime.now().hour, datetime.now().minute, datetime.now().day, datetime.now().month, datetime.now().year), reply_to_message_id=msg['message_id'])

    elif mensagem[0:8] == '/blabler' and mensagem[9:] != "" and '@' in mensagem and msg['from']['id'] == 175131873:
        adicionarAosBlablers(mensagem)

    elif mensagem[0:8] == '/blabler' and mensagem[9:] != "" and '@' in mensagem and msg['from']['id'] != 175131873:
        bot.sendMessage(chat_id, 'ACESSO NEGADO! Só o meu chefe pode fazer isso aí, ô estranho', reply_to_message_id=msg['message_id'])

    else:
        for frase in Falas_Enum.FALAS_JUNKRAT:
            if frase in mensagem.lower():
                bot.sendMessage(chat_id, random.choice(falas.RESPOSTAS_JUNKRAT), reply_to_message_id=msg['message_id'])



def definirChatId(chat_id):
    global chatID
    chatID = chat_id

def adicionarAosBlablers(username):
    #Pegar o nome do usuário que vier na mensagem após o comando /blabler
    novo_blabler = username[10:]
    #Abrir o arquivo criado
    arq = open('blablers.txt', 'r')
    conteudo = arq.readlines()
    blablers = str(conteudo).replace('\n','', 999)
    #Verificar se o usuário já está na lista dos blablers
    if novo_blabler not in blablers:
        arquivo = open('blablers.txt', 'w')
        conteudo.append(novo_blabler)
        conteudo.append("\n")
        arquivo.writelines(conteudo)
        arquivo.close()
        #Username Adicionado
        bot.sendMessage(chatID, 'Você adicionou %s' % novo_blabler)
    else:
        bot.sendMessage(chatID, 'Este usuário já está cadastrado!')


bot = telepot.Bot(TOKEN)
MessageLoop(bot, handle).run_as_thread()


while 1:
    time.sleep(1)
    now = datetime.now()
    # chatID = '-239108564'

    if now.hour == 7 and now.minute == 0 and now.second == 0:
        bot.sendVoice(chatID, voice=open('audios/bom_dia_camarada.mp3', 'rb'))

    if now.hour == 12 and now.minute == 0 and now.second == 0:
        bot.sendMessage(chatID, 'Mas aí, parças, vocês sabem que horas são?')
        bot.sendVoice(chatID, voice=open('audios/its_high_noon.mp3', 'rb'))

    if now.hour == 21 and now.minute == 0 and now.second == 0:
        bot.sendMessage(chatID, 'Anda, anda, anda, vamos jogar, camaradas!')

        # if now.minute == random.randrange(58, 59):
        #     bot.sendMessage(chatID, 'Pelo que eu vi aqui, colega, um viado tem esse tipo de foto de perfil:')
        #     foto = bot.getUserProfilePhotos(random.choice(BLABLERS))
        #     print(random.choice(BLABLERS))
        #     bot.sendPhoto(chatID, photo=foto)
