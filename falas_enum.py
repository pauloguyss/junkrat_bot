
class Falas_Enum:

    RESPOSTAS_ALEATORIAS = ['Como posso ficar aqui parado enquanto sei que tem coisas por ai esperando para serem explodiiiidaaaas?!',
                            'Só têm desajustados nisso aqui? Adorei!',
                            'Será que eu posso dar uma olhada nessas suas bombas?']

    FALAS_BEM_VINDO = ['e ai',
                       'ola',
                       'e ae',
                       'e aew']

    RESPOSTAS_BEM_VINDO = ['E aí, parça,Bem-vindo, camarada',
                           'E aí, comparsa, está pronto pro próximo plano?',
                           'Anda, anda, conte o plano!',
                           'Já decidiu o que vamos explodir agora?'
                           ]

    FALAS_BOM_DIA = ['bom dia',
                     'ou foda-se',
                     'bodia',
                     'bundinha']

    RESPOSTAS_BOM_DIA = ['O dia está ótimo para um pouco de caos!',
                         'Bom dia, camarada',
                         'Que ótimo dia, vamos explodir alguma coisa?',
                         'Pro dia ficar melhor só preciso encontrar aquele pacote de dinamites!']

    FALAS_JUNKRAT = ['ei, junk',
                     'junk']

    RESPOSTAS_JUNKRAT = ['Ei, você me chamou?',
                         'Também pode me chamar de Juimkrat',
                         'Eu estava terminando aquela bomba, o que você quer?',
                         'Não sei o que você precisa, mas vamos explodir logo isso',
                         'O que você quer agora?']

    FALAS_XINGAMENTOS = ['seu cu',
                         'seu cú',
                         'puta que o pariu',
                         'vai se foder',
                         'vai se fuder',
                         'se foder',
                         'se fuder',
                         'vá se foder',
                         'vá se fuder',
                         'bot lixo',
                         'fdp',
                         'filho da puta',
                         'fila da puta',
                         'vai tomar no seu cu',
                         'vai tomar no seu cú',
                         'vai tomar no teu cu',
                         'vai tomar no teu cú',
                         'vá tomar no cu',
                         'vá tomar no cú',
                         'vai tomar no cu',
                         'vai tomar no cú',
                         'se foda',
                         'arrombado',
                         'sua mãe',
                         'esse merda',
                         'enfia no cu',
                         'enfia no cú',
                         'seu bosta',
                         'cuzudo']

    RESPOSTAS_IMPOSTO = ['Imposto é roubo, camarada']

    RESPOSTAS_ROUBADAO = ['Ele é mesmo roubadão ou é apenas show da sua parte, colega?',
                          'O único roubadão que estou vendo é o imposto que a gente paga. Por isso eu ROUBO todo esse ouro!',
                          'Dei uma pesquisada aqui, camarada, e parece que o filme que mais se parece com você é o: EU, ROUBÔ',
                          'Acho que tá na hora de parar o papo, colega, não tá colando',
                          'Você está conversando com um espelho pra ficar chamando os outros de roubadão?',
                          'Aposto que ele não é tão roubado quanto alguém que já foi pro KAnadá, certo?',
                          'Telegram-API ERROR: Não consigo calcular a quantidade de roubo de quem enviou essa mensagem',
                          'Mas pelo que olhei, parceiro, só inventaram a palavra roubo depois que você apareceu!']

